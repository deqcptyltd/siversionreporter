package za.co.multichoice.simonitor;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.NetworkInterface;
import java.net.URL;
import java.util.Properties;

import org.apache.log4j.Logger;

import za.co.multichoice.descriptor.EntitlementIndicationDescriptor;
import za.co.multichoice.siprovider.Main;
import za.co.multichoice.util.CPUUtil;
import za.co.multichoice.util.DescriptorUtil;

// TODO: Auto-generated Javadoc
/**
 * The Class SIMonitor.
 * 
 * @author jonathanfrankel
 */
public class SIMonitor
{

    private static final String DVB_S = "dvb://";

    /** The ip count. */
    public static int IP_COUNT = 0;

    /**
     * The properties.
     * 
     */

    public static Properties properties = new Properties();

    /** The Constant log. */
    private final static Logger log = Logger.getLogger(SIMonitor.class);

    /** The Net int. */
    public static NetworkInterface NetInt = null;

    /**
     * The main method.
     * 
     * @param args the arguments
     */
    public static void main(String[] args)
    {

        try
        {

            try
            {

                DescriptorUtil.init();
                DescriptorUtil.descriptors.put(0xB0, EntitlementIndicationDescriptor.class);

                FileInputStream input = new FileInputStream(args[0]);
                properties.load(input);

                String grizzlyURI = properties.getProperty("grizzlyURI");
                if (grizzlyURI != null && grizzlyURI.trim().length() > 0)
                {
                    Main.BASE_URI = grizzlyURI;
                }
                log.debug("Starting: Main.BASE_URI: " + Main.BASE_URI);
                Main.startServer();

            } catch (FileNotFoundException ex)
            {
                log.warn(ex.getMessage(), ex);
            } catch (IOException ex)
            {
                log.warn(ex.getMessage(), ex);
            }

            log.debug("Init snap CPU");
            CPUUtil.getInstance().start();

            // Build the ip property from a url....
            buildIpList();

            String ni = properties.getProperty("networkInterface");
            if (ni != null && ni.trim().length() > 0)
            {
                NetInt = NetworkInterface.getByName(ni);
                log.debug("Using network interface name: " + ni);
            }

            // GitCommiter gitCommiter = new
            // GitCommiter(properties.getProperty("outputDirectory"));
            // gitCommiter.setup();
            //
            SIObserver siObserver = new SIObserver();
            if (properties.containsKey("filePath"))
            {
                siObserver.setup(properties.getProperty("filePath"));
            } else if (properties.containsKey("ip"))
            {
                if (properties.containsKey("networkInterface"))
                {
                    siObserver.setup(properties.getProperty("ip"), properties.getProperty("networkInterface"));
                }

                siObserver.setup(properties.getProperty("ip"));
            }
            siObserver.start();

        } catch (Exception ex)
        {
            log.error(ex.getMessage(), ex);
            throw new RuntimeException(ex);
        }
    }

    /**
     * Builds the ip list.
     */
    private static void buildIpList() throws Exception
    {
        String urlsP = properties.getProperty("url");

        if (urlsP == null || urlsP.trim().length() < 1)
        {
            return;
        }

        String[] urls = urlsP.split("\\;");

        String iplist = "";
        int i = 0;
        for (String url : urls)
        {
            iplist += (i > 0 ? ";" : "") + readFromURL(url);
            i++;
        }

        log.debug("iplist: " + iplist);

        properties.setProperty("ip", iplist);
    }

    /**
     * Read from url.
     * 
     * @param url the url
     * @return the string
     * @throws Exception the exception
     */
    private static String readFromURL(String url) throws Exception
    {
        String result = "";

        BufferedReader in = new BufferedReader(new InputStreamReader(new URL(url).openStream()));

        String inputLine;
        int cnt = 0;

        int from = Integer.parseInt(properties.getProperty("from"));
        int length = Integer.parseInt(properties.getProperty("length"));

        String dvb = "";

        while ((inputLine = in.readLine()) != null)
        {

            // log.debug(inputLine);

            if (!inputLine.trim().startsWith("#"))
            {

                int dvbStrt = inputLine.indexOf(DVB_S);
                if (dvbStrt > -1)
                {
                    dvb = inputLine.substring(dvbStrt + DVB_S.length(), inputLine.length());
                }

                int ff = inputLine.indexOf(".ip=");
                if (ff > -1)
                {

                    IP_COUNT += 1;

                    String ip = inputLine.substring(ff + 4, inputLine.length());
                    log.debug("got an ip: " + ip + " (" + IP_COUNT + ") ");

                    if (cnt >= from)
                    {
                        result += (cnt > 0 ? ";" : "") + ip;

                        if (inputLine.trim().toLowerCase()
                                .startsWith("stb.management.multicast.ip"))
                        {
                            result += ":1234" + ":" + dvb;
                        }

                    }
                }

                ff = inputLine.indexOf(".port=");
                if (ff > -1)
                {
                    String port = inputLine.substring(ff + 6, inputLine.length());
                    log.debug("Got a port: " + port);

                    if (cnt >= from)
                    {
                        result += ":" + port + ":" + dvb;
                    }
                    cnt++;

                }
            }

            if (length > 0 && cnt >= length)
            {
                break;
            }

        }

        in.close();

        return result;
    }
}
