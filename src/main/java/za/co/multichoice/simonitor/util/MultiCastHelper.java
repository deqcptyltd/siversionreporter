package za.co.multichoice.simonitor.util;

import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.MulticastSocket;
import java.util.concurrent.atomic.AtomicBoolean;

import org.apache.log4j.Logger;

import za.co.multichoice.simonitor.SIMonitor;

// TODO: Auto-generated Javadoc
/**
 * The Class MultiCastHelper.
 */
public class MultiCastHelper
{

    /** The Constant log. */
    private final static Logger log = Logger.getLogger(MultiCastHelper.class);

    /** The buffer. */
    private byte[] buffer = new byte[2048];

    /** The dgp. */
    private DatagramPacket dgp = null;

    /**
     * Checks if is IP accessible.
     * 
     * @param ip the ip
     * @param port the port
     * @return true, if is IP accessible
     */
    public boolean isIPAccessible(String ip, int port)
    {

        if (dgp == null)
        {
            dgp = new DatagramPacket(buffer, buffer.length);
        }

        // Try and connect, if connected and got something return immediately,
        // else wait 3 seconds, then return error.
        final IPAccess ipaccess = new IPAccess();

        ipaccess.ip = ip;
        ipaccess.port = port;

        new Thread(new Runnable()
        {

            @Override
            public void run()
            {

                try
                {
                    log.debug("\t\t testing " + ipaccess.ip);
                    MulticastSocket socket = new MulticastSocket(ipaccess.port);

                    log.debug("\t\t\t " + ipaccess.ip + " .....joining group...");
                    if (SIMonitor.NetInt != null)
                    {
                        socket.joinGroup(new InetSocketAddress(ipaccess.ip, ipaccess.port), SIMonitor.NetInt);
                    }
                    else
                    {
                        socket.joinGroup(InetAddress.getByName(ipaccess.ip));
                    }

                    ipaccess.socket = socket;

                    log.debug("\t\t\t " + ipaccess.ip + " .....receiving data...");
                    socket.receive(dgp);

                    ipaccess.gotData = true;

                    log.debug("\t\t\t " + ipaccess.ip + " .....leaving group...");
                    if (SIMonitor.NetInt != null)
                    {
                        socket.leaveGroup(new InetSocketAddress(ipaccess.ip, ipaccess.port), SIMonitor.NetInt);
                    }
                    else
                    {
                        socket.leaveGroup(InetAddress.getByName(ipaccess.ip));
                    }

                    log.debug("\t\t\t " + ipaccess.ip + " .....closing socket...");
                    socket.close();
                    ipaccess.socket = null;

                } catch (Exception ex)
                {
                    log.warn("Ip: " + ipaccess.ip + " " + ex.getMessage());
                } finally
                {
                    ipaccess.done.set(Boolean.TRUE);
                }

            }
        }).start();

        long time = System.currentTimeMillis();
        while (!ipaccess.done.get()
                && (System.currentTimeMillis() - time < 3000))
        {
            try
            {
                Thread.sleep(100);
                Thread.yield();
            } catch (Exception ex)
            {
                log.warn(ex.getMessage(), ex);
            }
        }

        try
        {

            if (ipaccess.socket != null)
            {
                ipaccess.socket.close();
            }
        } catch (Exception ex)
        {
            log.warn("Ip: " + ipaccess.ip + " " + ex.getMessage());

        }

        return ipaccess.gotData;
    }

    /**
     * The Class IPAccess.
     */
    private class IPAccess
    {

        /** The ip. */
        public String ip;

        /** The port. */
        public int port;

        /** The got data. */
        public boolean gotData = false;

        /** The socket. */
        public MulticastSocket socket = null;

        /** The done. */
        public AtomicBoolean done = new AtomicBoolean(Boolean.FALSE);
    }
}
