package za.co.multichoice.simonitor;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentSkipListMap;

import org.apache.log4j.Logger;

import za.co.multichoice.entity.BaseTable;
import za.co.multichoice.entity.TimeAndDateTable;
import za.co.multichoice.entity.TransportStreamDescriptionTable;
import za.co.multichoice.observer.BATObserver;
import za.co.multichoice.observer.CATObserver;
import za.co.multichoice.observer.DSTObserver;
import za.co.multichoice.observer.EITObserver;
import za.co.multichoice.observer.GeneralTableObserverWithTableID;
import za.co.multichoice.observer.NITObserver;
import za.co.multichoice.observer.Observable;
import za.co.multichoice.observer.PATObserver;
import za.co.multichoice.observer.PMTObserver;
import za.co.multichoice.observer.SDTObserver;
import za.co.multichoice.observer.TransportPacketObserver;
import za.co.multichoice.simonitor.util.MultiCastHelper;
import za.co.multichoice.state.TSDataManagementSystem;
import za.co.multichoice.streamer.MultiStreamer;
import za.co.multichoice.streamer.Streamer;
import za.co.multichoice.util.PrintStats;

/**
 * An asynchronous update interface for receiving notifications about SI
 * information as the SI is constructed.
 *
 * @author jonathanfrankel
 */
public class SIObserver implements Observable
{

    /** The Constant log. */
    private final static Logger log = Logger.getLogger(SIObserver.class);

    /** The mch. */
    private MultiCastHelper mch = new MultiCastHelper();

    // EIT -> EIT_123 -> 1 -> EIT_Object
    /** The base table version map. */
    private ConcurrentHashMap<String, ConcurrentHashMap<String, ConcurrentSkipListMap<Integer, BaseTable>>> baseTableVersionMap = new ConcurrentHashMap<>();

    /** The git commiters. */
    private Map<String, GitCommiter> gitCommiters = new HashMap<String, GitCommiter>();

    /** The streamers. */
    private List<MultiStreamer> streamers = new ArrayList<MultiStreamer>();

    /**
     * The network transponder ips. <br>
     * Only pick the first IP that connects from a given transponder. <br>
     * Key: NETWORK_TRANSPONDER
     * 
     */
    public static Map<String, String> NETWORK_TRANSPONDER_IPS = new HashMap<String, String>();

    /**
     * This method is called when information about an SI which was previously
     * requested using an asynchronous interface becomes available.
     */
    public SIObserver()
    {

    }

    /**
     * This method is called when information about an SI which was previously
     * requested using an asynchronous interface becomes available.
     *
     * @param options the options
     */
    public void setup(String... options)
    {
        try
        {

            MultiStreamer.NO_VAR_BUFFER = true;

            // MultiStreamer.expectedBitRate = 40960; //TODO: Remove this

            // Optionally set your Multicast buffer size
            // MultiStreamer.DESIRED_MULTICAST_BUFFER_SIZE = SOMETHING

            // =========================================== EXAMPLE
            // STREAMERS======================================================
            // Create new streamer to host:port.
            // MultiStreamer streamer = new MultiStreamer("1.2.3.4", 1234,
            // null);
            // Streamer multicast from specific Networkinterface
            // MultiStreamer streamer = new MultiStreamer("1.2.3.4", 1234,
            // NetworkInterface.getByName("eth0"));
            // Create a streamer from file...

            int cnt = 0;
            int connected = 0;

            switch (options.length)
            {
            case 1:
            {

                if (new File(options[0]).exists())
                {
                    MultiStreamer streamer = new MultiStreamer(new FileInputStream(options[0]));
                    if (SIMonitor.properties.containsKey("multicastBufferSize"))
                    {
                        streamer.DESIRED_MULTICAST_BUFFER_SIZE = Integer.parseInt(SIMonitor.properties.getProperty("multicastBufferSize"));
                    }

                    streamers.add(streamer);
                } else
                {
                    int invalid = 0;

                    String[] addresses = options[0].split("\\;");

                    double perc = 0.0d;
                    NumberFormat nf = NumberFormat.getInstance();
                    nf.setMaximumFractionDigits(2);
                    nf.setMinimumFractionDigits(0);

                    for (String address : addresses)
                    {

                        perc = ((double) cnt / (double) addresses.length) * 100.0d;

                        log.debug("checking address: " + address + " (" + nf.format(perc) + "%) ");
                        Thread.yield();

                        String adrsplit[] = address.split("\\:");
                        String ip = adrsplit[0];
                        Integer port = Integer.parseInt(adrsplit[1]);
                        String dvb = null;

                        if (adrsplit.length > 2)
                        {
                            dvb = adrsplit[2].substring(0, adrsplit[2].lastIndexOf("."));
                            // log.debug("\t dvb: " + dvb);
                        }

                        try
                        {

                            if ((dvb == null || NETWORK_TRANSPONDER_IPS.get(dvb) == null) && mch.isIPAccessible(ip, port))
                            {
                                if (dvb != null)
                                {
                                    NETWORK_TRANSPONDER_IPS.put(dvb, ip + ":" + port);
                                }

                                log.debug("\t is online? : " + address);
                                MultiStreamer streamer = new MultiStreamer(ip, port, SIMonitor.NetInt, 13160);

                                if (SIMonitor.properties.containsKey("multicastBufferSize"))
                                {
                                    streamer.DESIRED_MULTICAST_BUFFER_SIZE = Integer.parseInt(SIMonitor.properties.getProperty("multicastBufferSize"));
                                } else
                                {
                                    streamer.DESIRED_MULTICAST_BUFFER_SIZE = 13160;
                                }

                                streamer.initMonitor();
                                streamers.add(streamer);
                                connected += 1;
                            }
                            else
                            {
                                invalid += 1;
                            }

                        } catch (Exception ex)
                        {
                            log.warn(ex);
                        }

                        cnt += 1;

                        try
                        {
                            Thread.yield();
                            Thread.sleep(300);
                        } catch (Exception ex)
                        {
                            log.warn(ex.getMessage(), ex);
                        }

                    } // for

                    log.debug(" = = = = = =  = = = = = = = = = = IP ADDRESSES = = = = = = = = = ");

                    perc = ((double) connected / (double) addresses.length) * 100.0d;
                    log.debug("Connected to " + (connected) + " Out of " + addresses.length + " (" + nf.format(perc) + "%");
                    SIMonitor.IP_COUNT = connected;

                }

                break;
            }
            case 2:
            {

                String[] addresses = options[0].split("\\;");

                int invalid = 0;

                for (String address : addresses)
                {
                    Thread.yield();
                    log.debug("Splitting address: " + address);
                    String adrsplit[] = address.split("\\:");
                    String ip = adrsplit[0];
                    Integer port = Integer.parseInt(adrsplit[1]);
                    String dvb = null;

                    if (adrsplit.length > 2)
                    {
                        dvb = adrsplit[2].substring(0, adrsplit[2].lastIndexOf(".") - 1);
                        // log.debug("\t dvb: " + dvb);
                    }

                    try
                    {

                        if ((dvb == null || NETWORK_TRANSPONDER_IPS.get(dvb) == null) && mch.isIPAccessible(ip, port))
                        {
                            if (dvb != null)
                            {
                                NETWORK_TRANSPONDER_IPS.put(dvb, ip + ":" + port);
                            }

                            log.debug("\t is online? : " + address);
                            MultiStreamer streamer = new MultiStreamer(ip, port, SIMonitor.NetInt);
                            streamer.initMonitor();
                            streamers.add(streamer);
                            connected += 1;

                        }
                        else
                        {
                            invalid += 1;
                        }

                    } catch (Exception ex)
                    {
                        log.warn(ex);
                    }

                    cnt += 1;

                }

                log.debug(" = = = = = =  = = = = = = = = = = IP ADDRESSES = = = = = = = = = ");

                double perc = ((double) connected / (double) addresses.length) * 100.0d;
                NumberFormat nf = NumberFormat.getInstance();
                nf.setMaximumFractionDigits(2);
                nf.setMinimumFractionDigits(0);
                log.debug("Connected to " + (connected) + " Out of " + addresses.length + " (" + nf.format(perc) + "%");
                SIMonitor.IP_COUNT = connected;

                break;
            }
            // case 3: {
            // streamer = new MultiStreamer(options[0],
            // Integer.parseInt(options[1]),
            // NetworkInterface.getByName(options[2]));
            // streamers.add(streamer);
            // streamer.initMonitor();
            // break;
            // }
            default:
            {
                System.err.println(String.format("Cannot create a MultiStreamer with %d arguments. Exitting...", options.length));
                System.exit(-1);
                break;
            }
            }

            for (MultiStreamer streamer : streamers)
            {

                Thread.sleep(1);
                Thread.yield();

                // This will observe the buffer streamed from streamer, and
                // formulate transport packets.
                TransportPacketObserver tsobserver = new TransportPacketObserver();
                streamer.addObserver(tsobserver);

                // NIT
                NITObserver nitObserver = Streamer.addObserver(tsobserver,
                        NITObserver.class);

                // PAT
                PATObserver patObserver = Streamer.addObserver(tsobserver,
                        PATObserver.class);

                // PMT
                PMTObserver pmtObserver = Streamer.addObserver(tsobserver,
                        PMTObserver.class);

                // EIT
                EITObserver eitObserver = Streamer.addObserver(tsobserver,
                        EITObserver.class);

                // SDT
                SDTObserver sdtObserver = Streamer.addObserver(tsobserver, SDTObserver.class);

                // BAT
                BATObserver batObserver = Streamer.addObserver(tsobserver,
                        BATObserver.class);

                // CAT
                CATObserver catObserver = Streamer.addObserver(tsobserver,
                        CATObserver.class);

                // DST
                DSTObserver dstObserver = Streamer.addObserver(tsobserver,
                        DSTObserver.class);

                // TDT
                GeneralTableObserverWithTableID tsdtObserver = new
                        GeneralTableObserverWithTableID(0x02, 0x03,
                                TransportStreamDescriptionTable.class);
                Streamer.addObserver(tsobserver, tsdtObserver);

                // TSDT
                GeneralTableObserverWithTableID tdtObserver = new
                        GeneralTableObserverWithTableID(0x14, 0x70,
                                TimeAndDateTable.class);
                Streamer.addObserver(tsobserver, tdtObserver);

            }

            // The TSDataManagementSystem will now automatically be updated
            // as
            // tables are received.
            // To hook up to the TSDataManagementSystem, call:
            // must implment: za.co.multichoice.observer.Observable
            TSDataManagementSystem.getInstance().addObserver(new SIObserver());

        } catch (Exception ex)
        {
            log.error(ex.getMessage(), ex);
            throw new RuntimeException(ex.getMessage(), ex);
        }
    }

    /**
     * This method is called when information about an SI which was previously
     * requested using an asynchronous interface becomes available.
     */
    public void start()
    {
        // PROCESS

        boolean done = false;
        while (!done)
        {
            try
            {
                Thread.sleep(1);
                Thread.yield();
            } catch (Exception ex)
            {
                log.warn(ex.getMessage(), ex);
            }

            done = true;

            int index = 0;

            for (MultiStreamer streamer : streamers)
            {

                if (!streamer.getDone())
                {
                    done = false;
                }

                // optionally give other stuff a chance to process as well.
                Thread.yield();

                try
                {
                    // Call the main processing loop.
                    Thread.sleep(1);
                    // log.debug(" = = = = =  - - - - - = = = = = = = - - - -  - - ==  = = = = = =Streamer: "
                    // + index + " " + streamer.getIp());
                    streamer.process();
                } catch (Exception ex)
                {
                    log.warn(ex.getMessage(), ex);
                }

                index += 1;
            }
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * za.co.multichoice.observer.Observable#makeObservation(java.lang.Object[])
     */
    @Override
    public void makeObservation(Object... args)
    {
        try
        {

            Thread.yield();

            // GitCommiter gitCommiter = gitCommiters.get(args[1].toString());
            GitCommiter gitCommiter = gitCommiters.get("gitcommitter");
            if (gitCommiter == null)
            {
                // gitCommiter = new
                // GitCommiter(SIMonitor.properties.getProperty("outputDirectory")
                // + "/" + args[1].toString());
                gitCommiter = new GitCommiter(SIMonitor.properties.getProperty("outputDirectory"));
                gitCommiter.setup();

                gitCommiters.put("gitcommitter", gitCommiter);

            }

            if (args.length < 1)
            {
                return;
            }

            String className = args[0].getClass().getSimpleName();

            BaseTable baseTable = (BaseTable) args[0];
            String uniqueIdentifier = baseTable.getUniqueIdentifier();

            String shortenedId = className + "_" + uniqueIdentifier.substring(0, uniqueIdentifier.lastIndexOf("_"));
            ConcurrentSkipListMap<Integer, BaseTable> baseTables = null;
            BaseTable previousBaseTable = null;

            boolean versionChange = false;
            ConcurrentHashMap<String, ConcurrentSkipListMap<Integer, BaseTable>> baseTableType = baseTableVersionMap.get(className);
            if (baseTableType == null || baseTableType.get(shortenedId) == null)
            {
                if (baseTable.getSection_number().equals(0))
                {
                    versionChange = true;
                    if (baseTableType == null)
                    {
                        baseTableType = new ConcurrentHashMap<>();
                    }
                    baseTables = new ConcurrentSkipListMap<>();
                    baseTables.put(baseTable.getSection_number(), baseTable);
                    baseTableType.put(shortenedId, baseTables);
                    baseTableVersionMap.put(className, baseTableType);
                }
            } else
            {
                baseTables = baseTableType.get(shortenedId);
                previousBaseTable = baseTables.putIfAbsent(baseTable.getSection_number(), baseTable);
                if (previousBaseTable == null || !previousBaseTable.getVersion_number().equals(baseTable.getVersion_number()))
                {
                    versionChange = true;
                    baseTables.put(baseTable.getSection_number(), baseTable);
                    baseTableType.put(shortenedId, baseTables);
                    baseTableVersionMap.put(className, baseTableType);
                }
            }

            if (versionChange && baseTables != null && baseTables.size() == baseTables.firstEntry().getValue().getLast_section_number() + 1 && baseTable.getSection_number().equals(baseTable.getLast_section_number()))
            {
                // StringBuilder siTypes = new StringBuilder();
                // for (String siType : baseTableVersionMap.keySet()) {
                // if (siTypes.length() > 0)
                // siTypes.append(", ");
                //
                // siTypes.append(siType);
                // }
                // log.debug(String.format("There are %d types of object: %s",
                // baseTableVersionMap.size(), siTypes.toString()));
                // StringBuilder siUniqueIds = new StringBuilder();
                // for (String siUniqueId :
                // baseTableVersionMap.get(className).keySet()) {
                // if (siUniqueIds.length() > 0)
                // siUniqueIds.append(", ");
                //
                // siUniqueIds.append(siUniqueId);
                // }
                // log.debug(String.format("There are %d %s: %s",
                // baseTableVersionMap.get(className).size(), className,
                // siUniqueIds.toString()));
                // StringBuilder sectionNumbers = new StringBuilder();
                // for (Integer sectionNumber :
                // baseTableVersionMap.get(className).get(shortenedKey).keySet())
                // {
                // if (sectionNumbers.length() > 0)
                // sectionNumbers.append(", ");
                //
                // sectionNumbers.append(sectionNumber);
                // }
                // log.debug(String.format("There are %d sections numbered %s from %s",
                // baseTableVersionMap.get(className).get(shortenedKey).values().size(),
                // sectionNumbers.toString(), shortenedKey));
                // log.debug("");

                log.debug(String.format("Version number of %s set to: %d", className + "_" + uniqueIdentifier, baseTable.getVersion_number()));

                // This is why they pay you the big bucks, this is a bug mr J...
                // Checking all tables , and then only writing out the one that
                // changed.. tsk tsk tsk...
                // String printStats = PrintStats.printStats(args[0]);
                String printStats = "";

                for (BaseTable bt : baseTables.values())
                {
                    printStats += PrintStats.printStats(bt);
                }

                // String fle =
                // SIMonitor.properties.getProperty("outputDirectory") + "/" +
                // args[1].toString();
                String fle = SIMonitor.properties.getProperty("outputDirectory");

                try (BufferedWriter out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(fle + "/" + shortenedId + ".txt"), "UTF8")))
                {
                    out.write(printStats);
                    out.flush();
                    out.close();
                }

                String commitMessage = shortenedId + " version ";

                if (previousBaseTable != null)
                {
                    commitMessage += previousBaseTable.getVersion_number() + " changed to version " + baseTable.getVersion_number();
                } else
                {
                    commitMessage += baseTable.getVersion_number();
                }
                // log.debug(commitMessage);
                gitCommiter.commit(commitMessage);
            }

        } catch (Exception ex)
        {
            log.warn(ex);
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * za.co.multichoice.observer.Observable#addObserver(za.co.multichoice.observer
     * .Observable)
     */
    @Override
    public void addObserver(Observable observer)
    {
        // NOT REQUIRED FOR THIS CLASS
    }

    /**
     * This method is called when information about an SI which was previously
     * requested using an asynchronous interface becomes available.
     *
     * @param key the key
     * @return the SI table
     */
    public ConcurrentHashMap getSITable(String key)
    {
        return null;
        // return
    }
}

class BaseTableSectionNumberComparator implements Comparator<BaseTable>
{

    @Override
    public int compare(BaseTable o1, BaseTable o2)
    {
        return o1.getSection_number().compareTo(o2.getSection_number());
    }
}
