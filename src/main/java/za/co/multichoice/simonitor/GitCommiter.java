package za.co.multichoice.simonitor;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import org.apache.log4j.Logger;

// TODO: Auto-generated Javadoc
/**
 * The Class GitCommiter.
 *
 * @author jonathanfrankel
 */
public class GitCommiter
{

    /** The Constant log. */
    private final static Logger log = Logger.getLogger(GitCommiter.class);

    /** The output directory name. */
    private final String outputDirectoryName;

    /**
     * Instantiates a new git commiter.
     *
     * @param outputDirectory the output directory
     */
    public GitCommiter(String outputDirectory)
    {
        this.outputDirectoryName = outputDirectory;
    }

    /**
     * Setup.
     */
    public void setup()
    {
        File outputDirectory = new File(outputDirectoryName);
        outputDirectory.mkdirs();

        gitInitDirectory();

        gitIgnore();

        String script;
        script = "cd " + outputDirectoryName + "\n"
                + "git add -A\n"
                + "git commit -am \"%s\"\n"
                + "echo \"Committed with message %s\"\n";

        File scriptFile;

        if (isWindows())
        {
            // Message is mandatory and can never be AUTO -> It must show in
            // gitk what was the version change.
            // THe commit must fail, if it does not supply a version.

            script = "cd " + outputDirectoryName + "\r\n"
                    + "call git add -A\r\n"
                    + "call git commit -am %1\r\n"
                    + "echo \"Committed message: %1\"\r\n";
            scriptFile = new File(outputDirectoryName, "git_commiter.bat");
        }
        else
        {
            script = "#!/bin/bash\n" + script;
            script = String.format(script, "$1", "$1");
            scriptFile = new File(outputDirectoryName, "git_commiter.sh");
        }

        try
        {

            log.debug("\t writing git commit script file: " + scriptFile + " \r\n\t" + script);

            FileOutputStream fos = new FileOutputStream(scriptFile);
            fos.write(script.getBytes(), 0, script.length());
            fos.flush();
            fos.close();

        } catch (IOException ex)
        {
            log.warn(ex.getMessage(), ex);
        }

        if (!isWindows())
        {
            try
            {
                log.debug("\t\t Running GIT COMMITER");
                Process p = Runtime.getRuntime().exec(new String[] { "chmod", "+x", scriptFile.toString() });
                try
                {
                    Thread.yield();
                    Thread.sleep(3000);
                    p.waitFor();

                    outputScript(p);
                } catch (InterruptedException e)
                {
                    log.warn(e);
                }

            } catch (IOException ex)
            {
                log.warn(ex.getMessage(), ex);
            }
        }

    }

    /**
     * Commit.
     *
     * @param message the message
     */
    public void commit(String message)
    {
        String bFile = outputDirectoryName + "/git_commiter.sh";
        if (isWindows())
        {
            bFile = outputDirectoryName + "/git_commiter.bat";
        }

        try
        {
            log.debug("Running GIT COMMIT: " + bFile + " from " + new File(".").getCanonicalPath().toString());
            Process p = Runtime.getRuntime().exec(new String[] { bFile.toString(), "\"" + message + "\"" });
            Thread.yield();
            Thread.sleep(3000);
            p.waitFor();

            outputScript(p);

        } catch (IOException ex)
        {
            log.warn(ex.getMessage(), ex);
        } catch (InterruptedException ex)
        {
            log.warn(ex.getMessage(), ex);
        }

    }

    /**
     * Output script.
     *
     * @param p the p
     * @throws Exception the exception
     */
    private void outputScript(Process p)
    {
        try
        {
            InputStream is = p.getInputStream();
            String msg = " - - - - NO OUTPUT FROM SCRIPT - - - ";
            if (is.available() > 0)
            {
                byte[] buffer = new byte[is.available()];
                is.read(buffer, 0, buffer.length);
                msg = new String(buffer);
            }

            log.debug(" = = = SCRIPT RESULT = = = ");
            log.debug(msg);
            log.debug("\r\n\r\n");

        } catch (Exception ex)
        {
            log.warn(ex.getMessage(), ex);
        }

    }

    /**
     * Checks if is windows.
     *
     * @return true, if is windows
     */
    private boolean isWindows()
    {
        return System.getProperty("os.name").toLowerCase().startsWith("win");
    }

    /**
     * Git init directory.
     *
     * @return true, if successful
     */
    private boolean gitInitDirectory()
    {
        File gitDirectory = new File(outputDirectoryName, ".git");
        if (!gitDirectory.exists())
        {
            String script = "cd " + outputDirectoryName + "\n" +
                    "git init\n" +
                    "echo *.txt text > .gitattributes\n";

            File scriptFile;

            if (isWindows())
            {
                script = "cd " + outputDirectoryName + "\r\n"
                        + "call git init\n"
                        + "echo *.txt text > .gitattributes\n";
                scriptFile = new File(outputDirectoryName, "git_init.bat");
            } else
            {
                script = "#!/bin/bash\n" + script;
                scriptFile = new File(outputDirectoryName, "git_init.sh");
            }

            try
            {

                log.debug("\t writing git init script file: " + scriptFile + " \r\n\t" + script);

                FileOutputStream fos = new FileOutputStream(scriptFile);
                fos.write(script.getBytes(), 0, script.length());
                fos.flush();
                fos.close();

            } catch (IOException ex)
            {
                log.warn(ex.getMessage(), ex);
                return false;
            }

            if (!isWindows())
            {
                try
                {
                    log.debug("\t\t Running GIT INIT from " + scriptFile);
                    Process p = Runtime.getRuntime().exec(new String[] { "chmod", "+x", scriptFile.toString() });
                    Thread.yield();
                    Thread.sleep(3000);
                    p.waitFor();

                    outputScript(p);

                } catch (Exception ex)
                {
                    log.warn(ex.getMessage(), ex);
                    return false;
                }
            }

            try
            {
                log.debug("\t\t Running GIT INIT from " + scriptFile);
                Process p = Runtime.getRuntime().exec(scriptFile.toString());
                Thread.yield();
                Thread.sleep(3000);
                p.waitFor();

                outputScript(p);

            } catch (IOException ex)
            {
                log.warn(ex.getMessage(), ex);
                return false;
            } catch (InterruptedException ex)
            {
                log.warn(ex.getMessage(), ex);
            }

        }

        return true;
    }

    /**
     * Git ignore.
     */
    public void gitIgnore()
    {
        String gitIgnore = "*.sh\n*.bat";

        File gitIgnoreFile;

        gitIgnoreFile = new File(outputDirectoryName, ".gitignore");

        log.debug("\t writing output file: " + gitIgnoreFile + " \r\n\t" + gitIgnore);

        try
        {
            FileOutputStream fos = new FileOutputStream(gitIgnoreFile);
            fos.write(gitIgnore.getBytes(), 0, gitIgnore.length());
            fos.flush();
            fos.close();

        } catch (IOException ex)
        {
            log.warn(ex.getMessage(), ex);
        }
    }
}
